# 执行 cmd 命令

> 本文主要讲解 electron 如何执行 cmd 命令

通常我们有些功能是需要借助外部程序才能完成的，例如通过我们写的 electron 启动 nginx 或者获取本机的一些信息。

执行 cmd 命令不需要安装额外的依赖，使用 node 的 child_process 模块即可完成这个功能。

#### 1. 引用 child_process 模块

```js
import { exec } from "child_process";
```

#### 2. 执行 CMD 命令

这里我们执行一下 windows 常用命令 ipconfig，使用 child_process 执行 cmd 命令是可以指定执行的目录的（cmdPath）

```js
// 任何你期望执行的cmd命令，ls都可以
const cmdStr = "ipconfig";
// 执行cmd命令的目录，如果使用cd xx && 上面的命令，这种将会无法正常退出子进程
const cmdPath = pathUtil.getAppResourcePath("");

// 执行命令行，如果命令不需要路径，或就是项目根目录，则不需要cwd参数：
const workerProcess = exec(cmdStr, { cwd: cmdPath });
// 不受child_process默认的缓冲区大小的使用方法，没参数也要写上{}：workerProcess = exec(cmdStr, {})

// 打印正常的后台可执行程序输出
workerProcess.stdout.on("data", function (data) {
  console.log("stdout: " + data);
});

// 打印错误的后台可执行程序输出
workerProcess.stderr.on("data", function (data) {
  console.log("stderr: " + data);
});

// 退出之后的输出
workerProcess.on("close", function (code) {
  console.log("out code：" + code);
});
```

这个时候我们可以看到控制台输出的内容  
![](https://img-blog.csdnimg.cn/2020030316221262.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0RhdmlkMTAyNQ==,size_16,color_FFFFFF,t_70)  
我们可以看到输入的结果中，中文乱码，接下来我们来解决乱码，这里我们需要引入一个外部依赖 iconv-lite

#### 3. 安装 iconv-lite

```js
npm install iconv-lite --save
```

#### 4. 使用 iconv-lite 解决乱码

引入 iconv-lite

```js
const iconv = require("iconv-lite");
```

将原来的

```js
const workerProcess = exec(cmdStr, { cwd: cmdPath });
```

```js
console.log("stdout: " + data);
```

分别改为

```js
const workerProcess = exec(cmdStr, { cwd: cmdPath, encoding: "GBK" });
```

```js
console.log("stdout: " + iconv.decode(data, "GBK"));
```

这个时候我们再执行就正常了  
![](https://img-blog.csdnimg.cn/20200303162852302.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0RhdmlkMTAyNQ==,size_16,color_FFFFFF,t_70)
