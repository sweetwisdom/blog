# 什么是 electron?

> 使用 JavaScript，HTML 和 CSS 构建跨平台的桌面应用程序

![image-20220117094440407](http://store.cartoom.club//image-20220117094440407.png)

## 使用 electron 构建的软件有哪些?

> # 比你想象的更简单
>
> 如果你可以建一个网站，你就可以建一个桌面应用程序。 Electron 是一个使用 JavaScript, HTML 和 CSS 等 Web 技术创建原生程序的框架，它负责比较难搞的部分，你只需把精力放在你的应用的核心上即可。

![image-20220117094553138](http://store.cartoom.club//image-20220117094553138.png)

## 相关资源

[官方文档](https://www.electronjs.org/)

[官方示例仓库](https://github.com/electron/electron-quick-start)
